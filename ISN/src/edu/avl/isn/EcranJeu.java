package edu.avl.isn;

import java.io.IOException;
import java.util.ArrayList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import edu.avl.isn.creature.Personnage;
import edu.avl.isn.inventaire.EcranBoutique;
import edu.avl.isn.inventaire.EcranInventaire;

/**
 * @author alex Cette classe s'occupe a l'heure actuelle des interactions
 *         clavier, du personnage et de l'affichage du jeu en tant que tel
 */
public class EcranJeu extends BasicGameState {

	public static boolean nouveauJeu = false;

	private boolean quitter = false;

	// Id de la classe
	public static final int ID = 3;

	// vitesse de deplacement du personnage
	public static double vitessePersonnage = 0.1;

	// Attribution a chaque direction d'un chiffre
	public static final int haut = 0;
	public static final int gauche = 1;
	public static final int bas = 2;
	public static final int droite = 3;

	// tableau des valeurs à ajouter à x et y en fonction de la direction de la
	// creature
	public static final int decalageX[] = { 0, -1, 0, 1 };
	public static final int decalageY[] = { -1, 0, 1, 0 };

	// tableau indiquant la touche correspondant a la direction
	public static final int touches[] = { Input.KEY_UP, Input.KEY_LEFT,
			Input.KEY_DOWN, Input.KEY_RIGHT };

	public static int gold;

	// prochaine direction que prend le personnage lorsqu'on change de touche de
	// direction
	private int prochaineDirection;

	// Le personnage attaque a l'epee
	private boolean attackSword = false;

	// Le personnage attaque a l'arc
	private boolean attackBow = false;

	// le personnage attaque a la lance
	private boolean attackLance = false;

	// creation d'un container dans la fenetre
	@SuppressWarnings("unused")
	private GameContainer container;

	// position de notre personnage en pixel
	private float xPersonnage, yPersonnage;

	// étage de notre personnage
	private int zPersonnage;

	// prochaine position de notre personnage en case
	private int futurX, futurY;

	// direction du personnage
	private int direction = 0;

	// Le personnage se deplace ou non
	private boolean deplacement = false;

	// Le personnage s'arrete
	private boolean finirDeplacement = true;

	// La barre de vie/xp/mana
	private Hud hud;

	// On ouvre un objet, on prend un objet, on ferme un objet
	private boolean openObjet, keepObjet, closeObjet;

	// Initialisation de differentes variables objets
	private Niveau niveau;
	private Personnage personnage;
	private StateBasedGame game;

	private Sound[] tabSound = new Sound[6];

	private ArrayList<Niveau> niveaux;

	/**
	 * Fonction qui regarde si on doit avancer
	 */
	public void chekIfNeedMoving() {
		// Si la prochaine direction est la meme que celle actuelle et que le
		// personnage n'attaque pas on se deplace
		if (direction == prochaineDirection && !attackSword && !attackBow
				&& !attackLance) {
			this.deplacement = true;
			this.finirDeplacement = false;
			tabSound[0].play();
			preparerFutur();
		}
	}

	/**
	 * Fonction agissant lors de la pression sur une touche du clavier
	 * 
	 * @param key
	 *            La touche appuyee par l'utilisateur
	 * @param c
	 *            Le caractere correspondant a la touche appuyee
	 */
	public void keyPressed(int key, char c) {

		switch (key) {// action a faire en fonction de la touche pressee

		case Input.KEY_UP: // Si fleche vers le haut : deplacement vers le haut

			prochaineDirection = haut;
			chekIfNeedMoving();

			break;
		case Input.KEY_LEFT: // Si fleche vers la gauche : deplacement vers la
								// gauche
			prochaineDirection = gauche;
			chekIfNeedMoving();

			break;
		case Input.KEY_RIGHT: // Si fleche vers la droite : deplacement vers la
								// droite
			prochaineDirection = droite;
			chekIfNeedMoving();

			break;
		case Input.KEY_DOWN: // Si fleche vers le bas : deplacement vers le bas
			prochaineDirection = bas;
			chekIfNeedMoving();
			break;

		case Input.KEY_SPACE: // Si espace : attaque epee

			if (!attackBow && !attackLance) {
				tabSound[3].loop();
				attackSword = true;
				finirDeplacement = true;
			}
			deplacement = false;
			break;

		case Input.KEY_B: // Si b : attaque a l'arc
			if (!attackSword && !attackLance) {
				tabSound[2].loop();
				attackBow = true;
				finirDeplacement = true;
			}
			break;

		case Input.KEY_O: // Si o : ouvrir un objet
			openObjet = true;
			break;

		case Input.KEY_C: // Si c : fermer un objet
			closeObjet = true;
			break;

		case Input.KEY_K: // Si k : prendre un objet
			keepObjet = true;
			break;

		case Input.KEY_L: // Si l : attaque lance
			if (!attackBow || !attackSword) {
				tabSound[1].loop();
				attackLance = true;
			}
			finirDeplacement = true;
			break;

		case Input.KEY_I: // Si I: inventaire
			game.enterState(EcranInventaire.ID);
			break;
		case Input.KEY_E: // SI E: boutique
			game.enterState(EcranBoutique.ID);
			break;
		}
	}

	/**
	 * Fonction agissant lors de la relache d'une touche du clavier
	 * 
	 * @param key
	 *            La touche relachee par l'utilisateur
	 * @param c
	 *            Le caractere correspondant a la touche relachee
	 * 
	 */
	public void keyReleased(int key, char c) {
		// On s'arrete
		if (touches[direction] == key) {
			this.finirDeplacement = true;
		}

		// On quitte
		if (Input.KEY_ESCAPE == key) {
			quitter = true;
		}

		// On arrete d'attaquer a l'epee
		if (Input.KEY_SPACE == key) {
			tabSound[3].stop();
			attackSword = false;
		}

		// On arrete d'attaquer a l'arc
		if (Input.KEY_B == key) {
			tabSound[2].stop();
			attackBow = false;
		}

		// On arrete d'attaquer a la lance
		if (Input.KEY_L == key) {
			tabSound[1].stop();
			attackLance = false;
		}
	}

	@SuppressWarnings("static-access")
	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {

		this.game = game;
		zPersonnage = 0;
		niveaux = new ArrayList<Niveau>();

		// si on doit faire un nouveau Jeu ou que le chargement de jeu ne marche
		// pas on génère une nouvelle liste de niveau, sinon on charge la liste
		// a partir du fichier
		if (nouveauJeu) {
			nouveauJeu = false;
			niveau = new Niveau(zPersonnage);
			niveaux.add(niveau);
		} else {
			try {
				niveaux = Sauvegarde.restaureNiveau();
				niveau = niveaux.get(zPersonnage);
			} catch (ClassNotFoundException | IOException e) {
				niveau = new Niveau(zPersonnage);
				niveaux.add(niveau);
			}
		}

		// On genere le personnage
		personnage = new Personnage();
		xPersonnage = niveau.entreeX * 32;
		yPersonnage = niveau.entreeY * 32;
		futurX = niveau.entreeX;
		futurY = niveau.entreeY;
		this.gold = personnage.getMoney();
		hud = new Hud();

		// On initialise les musics
		Music background = new Music(
				"sprite/musique/David_Saulesco_-_Eternal_Daughter_Original_Soundtr.ogg");
		background.setVolume(0.0f);
		background.loop();
		background.setVolume(0.2f);
		chargerSons();
	}

	/**
	 * 
	 * @return the nouveauJeu
	 */
	public static boolean isNouveauJeu() {
		return nouveauJeu;
	}

	/**
	 * 
	 * @param nouveauJeu
	 *            the nouveauJeu to set
	 */
	public static void setNouveauJeu(boolean nouveauJeu) {
		EcranJeu.nouveauJeu = nouveauJeu;
	}

	/**
	 * Affiche les differentes couche de jeu : le donjon, les objets, les
	 * creatures, le personnage et la barre de vie
	 */
	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {

		niveau.render(container, g, xPersonnage, yPersonnage);

		personnage.render(container, g, xPersonnage, yPersonnage, direction,
				deplacement, attackSword, attackBow, attackLance);

		hud.render(g, personnage);

	}

	/**
	 * Calcul la prochaine case atteinte par le personnage en fonction de sa
	 * direction et regarde si il doit s'arreter
	 */
	private void preparerFutur() {
		int testX = futurX;
		int testY = futurY;
		testX += decalageX[direction];
		testY += decalageY[direction];
		if (!niveau.estAccessible(testX, testY)) {
			finirDeplacement = true;
		}
		if (finirDeplacement) {
			deplacement = false;
			tabSound[0].stop();
		}
		if (deplacement) {
			futurX = testX;
			futurY = testY;
		}
	}

	/**
	 * Met a jour les element du jeu en fonction du delta temps qui est survenu
	 */
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {

		// On quitte l'ecran de jeu
		if (quitter) {
			quitter = false;
			Sauvegarde.saveNiveau(niveaux);
			game.enterState(EcranMenuPrincipal.ID);
		}

		boolean limiteAtteinte = false;

		// Met a jour les nouvelles coordonnees du personnage, et regarde si il
		// a atteint une nouvelle case
		if (this.deplacement) {
			switch (direction) {
			case gauche:
				this.xPersonnage -= delta * vitessePersonnage;
				if (this.xPersonnage <= futurX * 32) {
					limiteAtteinte = true;
				}
				break;
			case droite:
				this.xPersonnage += delta * vitessePersonnage;
				if (this.xPersonnage >= futurX * 32) {
					limiteAtteinte = true;
				}
				break;
			case haut:
				this.yPersonnage -= delta * vitessePersonnage;
				if (this.yPersonnage <= futurY * 32) {
					limiteAtteinte = true;
				}
				break;
			case bas:
				this.yPersonnage += delta * vitessePersonnage;
				if (this.yPersonnage >= futurY * 32) {
					limiteAtteinte = true;
				}
				break;
			}

			if (limiteAtteinte) {
				// Si le doit personnage doit tourner ou finir son deplacement
				// sur la nouvelle case, alors on donne a ses coordonnees celles
				// de la case et on met a jour sa direction
				if (prochaineDirection != direction || finirDeplacement) {
					xPersonnage = futurX * 32;
					yPersonnage = futurY * 32;
					direction = prochaineDirection;
				}
				
				//Si on a atteint une case de sortie alors on monte d'un niveau
				if (niveau.estSortie((int) xPersonnage / 32,
						(int) yPersonnage / 32)) {
					zPersonnage++;
					if (zPersonnage == niveaux.size()) {
						niveau = new Niveau(zPersonnage);
						niveaux.add(niveau);
					} else {
						niveau = niveaux.get(zPersonnage);
					}
					xPersonnage = niveau.entreeX * 32;
					yPersonnage = (niveau.entreeY + 2) * 32 - 32;
					futurX = niveau.entreeX;
					futurY = niveau.entreeY + 1;
					finirDeplacement = true;
					prochaineDirection = bas;
				}
				
				//Si on a atteint une case de sortie alors on descend d'un niveau
				if (niveau.estEntree((int) xPersonnage / 32,
						(int) yPersonnage / 32) && zPersonnage > 0) {
					zPersonnage--;
					niveau = niveaux.get(zPersonnage);
					xPersonnage = niveau.sortieX * 32;
					yPersonnage = (niveau.sortieY + 2) * 32 - 32;
					futurX = niveau.sortieX;
					futurY = niveau.sortieY + 1;
					finirDeplacement = true;
					prochaineDirection = bas;
				}
				
				//On regarde quelle sera la prochaine case atteinte.
				preparerFutur();
			}
		} else {
			direction = prochaineDirection;
		}

		// On ouvre un objet
		if (openObjet) {
			boolean play;
			// On appel la fonction ouvrir de objet via niveau
			play = niveau.ouvrir((int) xPersonnage / 32 + decalageX[direction],
					(int) yPersonnage / 32 + decalageY[direction]);
			if (play) {
				tabSound[4].play();
			}
			openObjet = false;
		}

		// On ferme un objet
		if (closeObjet) {
			// On appel la fonction fermer de objet via niveau
			boolean play = niveau.fermer((int) xPersonnage / 32 + decalageX[direction],
					(int) yPersonnage / 32 + decalageY[direction]);
			if (play) {
				tabSound[5].play();
			}
			closeObjet = false;
		}

		// On prend un objet
		if (keepObjet) {
		}
	}

	// Initialisation de la liste des sons
	private void chargerSons() throws SlickException {
		tabSound[0] = new Sound("sprite/sons/pave.ogg");
		tabSound[1] = new Sound("sprite/sons/ahh.ogg");
		tabSound[2] = new Sound("sprite/sons/arc.ogg");
		tabSound[3] = new Sound("sprite/sons/epee.ogg");
		tabSound[4] = new Sound("sprite/sons/openCoffre.ogg");
		tabSound[5] = new Sound("sprite/sons/closeCoffre.ogg");
	}

	@Override
	public int getID() {
		return ID;
	}

	public Personnage getPersonnage() {
		return personnage;
	}

	public static int getPersoMoney() {
		return gold;
	}
}