package edu.avl.isn.objet;

import java.io.Serializable;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

@SuppressWarnings("serial")
public abstract class Objet implements Serializable{
		
	static SpriteSheet objetSprite;
	
	public Objet(){
		
	}
	
	public void ouvrir(){
 		
 	}
	
	public void fermer(){
		
	}
	
	public void ramasser(){
		
	}
	
	public void interragir(){
		
	}
	
	public void poser(){
		
	}
	
	public boolean estAccessible(){
		return false;
	}
	
	public abstract void render(GameContainer container, Graphics g, int x, int y, SpriteSheet objetSprite);
	
	static public void init() throws SlickException{
		//objetSprite = new SpriteSheet("sprite/objet.png", 32, 32);
	}
}
