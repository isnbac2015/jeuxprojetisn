package edu.avl.isn.objet;



import java.util.ArrayList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SpriteSheet;

import edu.avl.isn.inventaire.Obj;

@SuppressWarnings("serial")
public class Coffre extends Objet{

	
	private ArrayList<Obj> contenuCoffre;
	private boolean ouvert = false;

	public Coffre() {
		super();
		setContenuCoffre(new ArrayList<Obj>());
		// Ajout dans un cofrre
		// contenuCoffre.add(new Epee("Test", "test", 1, 1, 5));
	}
	
	public void render(GameContainer container, Graphics g, int x, int y, SpriteSheet objetSprite) {
		objetSprite.startUse();
				if(ouvert)
					objetSprite.renderInUse(x * 32, y * 32, 0, 1);
				else
					objetSprite.renderInUse((int)x * 32,(int) y * 32, 0, 0);
		objetSprite.endUse();
	}
	
	public void ouvrir() {
		ouvert = true;
		System.out.println("coffre ouvert " );
	}
	
	public void fermer() {
		ouvert = false;
		System.out.println("coffre ferme " );
	}

	public ArrayList<Obj> getContenuCoffre() {
		return contenuCoffre;
	}

	public void setContenuCoffre(ArrayList<Obj> contenuCoffre) {
		this.contenuCoffre = contenuCoffre;
	}
}
