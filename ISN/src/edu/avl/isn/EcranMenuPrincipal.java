package edu.avl.isn;

import java.awt.Font;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;



/**
 * Gere l'ecran principal
 * @author vgalves
 *
 */
public class EcranMenuPrincipal extends BasicGameState {
	public static final int ID = 5;

	private boolean quitter = false;
	
	private StateBasedGame game;
	
	//Les position de debut de la ligne, la colonne et l'espace entre les cases
	private int startx = 250, posy = 200, espacementX = 150;
	
	//Le tableau de case
	private Case[] tabCase = new Case[3];
	
	//La police
	private Font font;
	private TrueTypeFont ttf;
	
	//L'image du fond d'ecran
	private Image fondEcran;
	
	private Sound sonsSourisSurCase;
	

	/**
	 * Initialise l'ecran
	 */
	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {
		this.game = game;
		
		//On initialise notre tableau de case
		this.tabCase[0] = new Case(startx, posy, "Nouvelle Partie",
				"sprite/ImageMenuPrin/new.png", 3);
		
		this.tabCase[1] = new Case(startx + espacementX, posy,
				"Charger Partie", "sprite/ImageMenuPrin/load.png", 3);
		
		this.tabCase[2] = new Case(startx + (2 * espacementX), posy, "Quitter",
				"sprite/ImageMenuPrin/x.png", 6);
		
		//Puis la police qu'on utilise
		font = new Font("Verdana", Font.BOLD, 15);
		ttf = new TrueTypeFont(font, true);

		this.fondEcran = new Image("sprite/ImageMenuPrin/image fond.jpg");
		
		sonsSourisSurCase = new Sound("sprite/sons/sonsBlop2.ogg");
	}

	/**
	 * Rend l'ecran
	 */
	@Override
	public void render(GameContainer arg0, StateBasedGame arg1, Graphics g)
			throws SlickException {
		//On affiche l'image du fond
		g.drawImage(fondEcran, 0, 0);
		
		//On dit qu'on est sur le menu principal
		ttf.drawString(30, 30, "Menu Principal", Color.cyan);
		
		//Puis on affiche chaque case
		for (int i = 0; i < tabCase.length; i++) {
			tabCase[i].renderSingleCase(g, ttf);
		}
	}

	/**
	 * Met a jour l'ecran
	 */
	@Override
	public void update(GameContainer container, StateBasedGame arg1, int arg2)
			throws SlickException {
		if(quitter){
			quitter = false;
			container.exit();
		}
		if(isOn(Mouse.getX(), Mouse.getY())){
			Mouse.updateCursor();
		}
	}
	
	
/**
 * Renvoi l'id de l'ecran
 */
	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return ID;
	}
	
	/**
	 * Rafraichit le isMouseOn de chaque case lorsqu'on bouge la souris
	 */
	@Override
	public void mouseMoved(int oldx, int oldy, int newx, int newy) {
		mouseOn(newx, newy);
	}
	
	/**
	 * Permet de changer d'ecran lorsqu'on clique sur un case
	 */
	@Override
	public void mousePressed(int button, int x, int y) {
		for (int i = 0; i < tabCase.length; i++) {
			if (x >= tabCase[i].posx && x <= tabCase[i].posx + espacementX 
					&& y >= this.posy && y <= this.posy + espacementX){
				if(tabCase[i].IDto == 6){
					quitter = true;
				} else {
					if(i == 0){
						EcranJeu.setNouveauJeu(true);
					}
					game.enterState(tabCase[i].IDto);
				}
			}
			
		}
	}
	
	/**
	 * Change le boolean isMoseOn de la case sur lequel la souris est
	 * @param posx
	 * @param posy
	 */
	public void mouseOn (int posx, int posy){
		//On parcour le tableau de case
		for (int i = 0; i < tabCase.length; i++) {
			//Si la souris est dessus on indique que la souris y est
			if (posx >= tabCase[i].posx && posx <= tabCase[i].posx + espacementX 
					&& posy >= this.posy && posy <= this.posy + espacementX){
				if (!tabCase[i].isMouseOn) {
					sonsSourisSurCase.play();
				}
				
				tabCase[i].setMouseOn(true);
			}
			else {
				tabCase[i].setMouseOn(false);
			}
		}
	}
	
	/**
	 * verifie si la souris est sur une case
	 * @param posx
	 * @param posy
	 * @return if the mouse is on
	 */
	public boolean isOn(int posx, int posy){
		//On parcour le tableau de case
				for (int i = 0; i < tabCase.length; i++) {
					//Si la souris est dessus on indique que la souris y est
					if (posx >= tabCase[i].posx && posx <= tabCase[i].posx + espacementX 
							&& posy >= this.posy && posy <= this.posy + espacementX){
						return true;
					}
				}
				return false;
	}
	
	/**
	 * Une case de l'ecran
	 * @author vicga_000
	 *
	 */
	
	class Case{
		//Coin superieur droit de la case
		private int posx, posy;
		//L'image de la case
		private Image im;
		//Son nom
		private String name;
		//Si la souris est dessus
		private boolean isMouseOn;
		//Le state game qui viendra un fois qu'on aura clicker dessus
		private int IDto;
		
		
		/**
		 * Construit un nouvelle case
		 * @param posx la coordonnee x du point superieur gauche
		 * @param posy la coordonnee y du coin superieur gauche
		 * @param name le nom de la case
		 * @param ref the image ref
		 * @throws SlickException si l'image n'est pas trouvee
		 */
		public Case(int posx, int posy, String name, String ref, int to) throws SlickException {
			super();
			this.posx = posx;
			this.posy = posy;
			this.name = name;
			this.im = new Image(ref);
			this.isMouseOn = false;
			this.IDto = to;
		}
		/**
		 * rend une case
		 * @param g
		 * @param ttf
		 */
		public void renderSingleCase(Graphics g, TrueTypeFont ttf){
			//Si la souris est dessu, on dessine un rectangle autour
			if (isMouseOn) {
				g.setColor(Color.white);
				g.drawRect(posx - 5, posy - 5, 148, 148);
			}
			//Puis on affiche l'image et ce qu'elle fait
			g.drawImage(im, posx, posy);
			ttf.drawString(posx, posy + espacementX, name);
		}

		/**
		 * set si la souris est dessus
		 * @param isMouseOn the isMouseOn to set
		 */
		public void setMouseOn(boolean isMouseOn) {
			this.isMouseOn = isMouseOn;
		}
		
		
	}

}
