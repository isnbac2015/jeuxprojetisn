package edu.avl.isn;

import java.io.Serializable;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

import edu.avl.isn.objet.*;

@SuppressWarnings("serial")
public class Niveau implements Serializable {

	private int etage;
	static int tailleMapX = 40;
	static int tailleMapY = 40;

	private String imageObjet, imageBloc;
	private transient SpriteSheet objetSprite, blocSprite;

	private Objet[][] mapObjet;
	private Map map;
	// creation d'un objet map : la carte qui sera affiche
	public int entreeX, entreeY, sortieX, sortieY;

	public Niveau(int etage) throws SlickException {
		this.etage = etage;
		Level level = new Level(tailleMapX / 2, tailleMapY / 2, 3);
		map = new Map(level, tailleMapX, tailleMapY, etage);
		mapObjet = new Objet[tailleMapY][tailleMapX];
		genererObjet(level);
		entreeX = map.entreeX;
		entreeY = map.entreeY;
		sortieX = map.sortieX;
		sortieY = map.sortieY;
		imageObjet = "sprite/objet.png";
		imageBloc = "sprite/fond.png";
		objetSprite = new SpriteSheet(imageObjet, 32, 32);
		blocSprite = new SpriteSheet(imageBloc, 32, 32);
	}

	public boolean estSortie(int x, int y) {
		return (x / 2 == sortieX / 2 && y / 2 == sortieY / 2);
	}
	
	public boolean estEntree(int x, int y) {
		return (x / 2 == entreeX / 2 && y / 2 == (entreeY - 1) / 2);
	}
	

	public boolean estAccessible(int x, int y) {
		return map.estAccessible(x, y)
				&& (mapObjet[y][x] == null || mapObjet[y][x].estAccessible());
	}

	public void render(GameContainer container, Graphics g, float x, float y)
			throws SlickException {
		// map.render(container, g, x, y);
		// renderMapObjet(container, g);

		map.render(container, g, x, y, blocSprite);
		renderMapObjet(container, g, objetSprite);
	}

	public boolean ouvrir(int x, int y) {
		if (mapObjet[y][x] != null) {
			mapObjet[y][x].ouvrir();
			return true;
		}
		else
			return false;
	}

	public boolean fermer(int x, int y) {
		if (mapObjet[y][x] != null) {
			mapObjet[y][x].fermer();
			return true;
		}
		else
			return false;
	}

	public void genererObjet(Level level) {
		for (int i = 0; i < level.listeSalle.size(); i++) {
			Level.Salle salle = level.listeSalle.get(i);

			// Creation d'un coffre au centre de la salle pour les tests.
			int xCoffre = (salle.x1 + salle.x2) / 2;
			int yCoffre = (salle.y1 + salle.y2) / 2;

			mapObjet[yCoffre][xCoffre] = new Coffre();

		}
	}

	public void renderMapObjet(GameContainer container, Graphics g,
			SpriteSheet blocSprite) {
		for (int j = 0; j < tailleMapY; j++) {
			for (int i = 0; i < tailleMapY; i++) {
				if (mapObjet[j][i] != null)
					mapObjet[j][i].render(container, g, i, j, blocSprite);
			}
		}
	}

	public void setObjetSprite() throws SlickException {
		objetSprite = new SpriteSheet(imageObjet, 32, 32);
	}

	public void setBlocSprite() throws SlickException {
		blocSprite = new SpriteSheet(imageBloc, 32, 32);
	}

}