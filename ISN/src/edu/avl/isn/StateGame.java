package edu.avl.isn;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 * @author vgalves
 * 
 * Cette classe est la classe qui gere les diff�rentes "fenetre" du jeu.
 */


import edu.avl.isn.inventaire.EcranBoutique;
import edu.avl.isn.inventaire.EcranInventaire;

public class StateGame extends StateBasedGame {

	public StateGame(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Permet de mettre dans le jeu les differents ecrans
	 */
	
	@Override
	public void initStatesList(GameContainer arg0) throws SlickException {
		// TODO Auto-generated method stub
		addState(new EcranAccueil());
		addState(new EcranJeu());
		addState(new EcranBoutique());
		addState(new EcranInventaire());
		addState(new EcranMenuPrincipal());	
	}

	

}
