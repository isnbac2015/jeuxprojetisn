package edu.avl.isn;
import java.awt.Font;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;

import edu.avl.isn.creature.Personnage;


public class Hud {
	private Image playerbars;
	
	private static final int P_BAR_X = 10;
	private static final int P_BAR_Y = 10;
	
	private static final int BAR_X = 84 + P_BAR_X;
	private static final int LIFE_BAR_Y = 4 + P_BAR_Y;
	private static final int MANA_BAR_Y = 24 + P_BAR_Y;
	private static final int XP_BAR_Y = 44 + P_BAR_Y;
	private static final int BAR_WIDTH = 80;
	private static final int BAR_HEIGHT = 16;

	private static final Color LIFE_COLOR = new Color(255, 0, 0);
	private static final Color MANA_COLOR = new Color(0, 0, 255);
	private static final Color XP_COLOR = new Color(0, 255, 0);
	private Font font;
	private TrueTypeFont ttf;

	public Hud () throws SlickException{
		init();
	}
	
	 public void init() throws SlickException {
		    this.playerbars = new Image("sprite/player-bar.png");
		    font = new Font("Verdana", Font.BOLD, 32);
		    ttf = new TrueTypeFont(font, true);
		  }
	 
	 public void render(Graphics g, Personnage perso) {
		  g.resetTransform();
		  g.setColor(LIFE_COLOR);
		  g.fillRect(BAR_X, LIFE_BAR_Y, (perso.getVie()/100) * BAR_WIDTH, BAR_HEIGHT);
		  g.setColor(MANA_COLOR);
		  g.fillRect(BAR_X, MANA_BAR_Y, (perso.getMana()/100) * BAR_WIDTH, BAR_HEIGHT);
		  g.setColor(XP_COLOR);
		  g.fillRect(BAR_X, XP_BAR_Y, (perso.getXp()/100) * BAR_WIDTH, BAR_HEIGHT);
		  g.drawImage(this.playerbars, P_BAR_X, P_BAR_Y);
		  ttf.drawString(30, 20, String.valueOf(perso.getMoney()), Color.yellow);
		}

}
