package edu.avl.isn;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Gere l'ecran d'accueil
 * @author vgalves
 *
 */
public class EcranAccueil extends BasicGameState {
	public static final int ID = 1;
	private StateBasedGame game;
	
	/**
	 * Initialise l'ecran
	 */
	@Override
	public void init(GameContainer arg0, StateBasedGame game)
			throws SlickException {
		this.game = game;
	}

	/**
	 * Rend l'ecran
	 */
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {
		
		g.drawString("Appuyer sur n'importe quelle touche", 350, 350);
		
	}

	/**
	 * Mise a jour de l'ecran
	 */
	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Retourne l'id de l'ecran
	 */
	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return ID;
	}
	
	/**
	 * Action si une touche est relachee
	 */
	@Override
	public void keyReleased(int key, char c) {
		game.enterState(EcranMenuPrincipal.ID);
	}

}