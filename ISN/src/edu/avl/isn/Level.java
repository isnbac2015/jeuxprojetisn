package edu.avl.isn;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

/**
 * @author alex
 *
 */
@SuppressWarnings("serial")
public class Level implements Serializable {

	// taille de la map a generer
	private int tailleHauteur, tailleLargeur;

	// map de l'etage
	private char[][] levelMap;

	public static char blocMur = '#';
	public static char blocSol = ' ';
	public static char blocEscalierEntree = 'M';
	public static char blocEscalierSortie = 'D';

	Random rand = new Random();

	// Liste des salles de l'etage
	public ArrayList<Salle> listeSalle = new ArrayList<Salle>();

	/**
	 * Fonction generant un nombre aleatoire
	 * 
	 * @param min
	 *            Minimum
	 * @param max
	 *            Maximum
	 * @return Le nombre aleatoire
	 */
	public int aleatoire(int min, int max) {
		return rand.nextInt(max - min + 1) + min;
	}

	/**
	 * @author alex Class des objets salles, une salle est un rectangle defini
	 *         par les coordonnees de deux de ses angles. Les murs de la salle
	 *         sont compris dans cele-ci.
	 */
	public class Salle {
		public int x1, y1, x2, y2;

		public Salle(int _x1, int _y1, int _x2, int _y2) {
			x1 = _x1;
			x2 = _x2;
			y1 = _y1;
			y2 = _y2;

		}
	}

	/**
	 * Constructeur de Level
	 * 
	 * @param hauteur
	 *            Hauteur y de chaque etage du donjon
	 * @param largeur
	 *            Largeur x de chaque etage du donjon
	 * @param minSize
	 *            Taille minimum en largeur et longueur des salles qui seront
	 *            dans chaque etage
	 */
	public Level(int hauteur, int largeur, int minSize) {
		long rgenseed = System.currentTimeMillis();
		System.out.println("Random number generator seed is " + rgenseed + "L");
		rand.setSeed(rgenseed);
		tailleHauteur = hauteur;
		tailleLargeur = largeur;
		levelMap = new char[tailleHauteur][tailleLargeur];
		remplissage('#');

		divise(0, 0, tailleLargeur - 1, tailleHauteur - 1, minSize + 2);

		generateEntree();
		generateSortie();
		// generateObjetCoffre();

	}

	/**
	 * Fonction de test : Permet d'afficher dans la console l'ensemble du
	 * labyrinthe genere par level
	 */
	public void affichageLevel() {
		// affichage des caracteristiques de generation du labyrinthe
		System.out.println("Hauteur = " + tailleHauteur + ", Largeur = "
				+ tailleLargeur);

		// On parcours une ligne puis puis on l'affiche
		String ligne;
		for (int i = 0; i < tailleHauteur; i++) {
			ligne = "";
			for (int j = 0; j < tailleLargeur; j++) {
				ligne = ligne + String.valueOf(levelMap[i][j])
						+ String.valueOf(levelMap[i][j]);
			}
			System.out.println(ligne);
		}
	}

	/**
	 * Permet de remplir une partie d'une matrice par un caractere
	 * 
	 * @param caractere
	 *            Le caractere dont l'on souhaite remplir le tableau
	 * @param coordonneeX
	 *            Coordonnee X d'où part le remplissage
	 * @param coordonneeY
	 *            Coordonnee Y d'où part le remplissage
	 * @param dimensionHauteur
	 *            Hauteur du rectangle que l'on souhaite remplir
	 * @param dimensionLargeur
	 *            Largeur du rectangle que l'on souhaite remplir
	 */
	public void remplissage(char caractere, int coordonneeX, int coordonneeY,
			int dimensionHauteur, int dimensionLargeur) {

		// On change un a un chaque caractere du tableau en parcourant ligne par
		// ligne le rectange que l'on souhaite remplir du caractere
		if (dimensionHauteur + coordonneeX <= tailleHauteur
				&& dimensionLargeur + coordonneeY <= tailleLargeur) {
			for (int i = coordonneeX; i < dimensionHauteur + coordonneeX; i++) {
				for (int j = coordonneeY; j < dimensionLargeur + coordonneeY; j++) {
					levelMap[j][i] = caractere;
				}
			}
		} else {
			System.out.println("Erreur, remplissage impossible");
		}
	}

	/**
	 * Permet de changer le caractere d'une case du tableau
	 * 
	 * @param caractere
	 *            Le nouveau caractere
	 * @param coordonneeX
	 *            Coordonnee X de la case visee
	 * @param coordonneeY
	 *            Coordonnee Y de la case visee
	 */
	public void remplissage(char caractere, int coordonneeX, int coordonneeY) {
		levelMap[coordonneeY][coordonneeX] = caractere;
	}

	/**
	 * extension de remplissage : remplit l'ensemble du tableau
	 * 
	 * @param caractere
	 */
	public void remplissage(char caractere) {
		remplissage(caractere, 0, 0, tailleHauteur, tailleLargeur);
	}

	/**
	 * Algorithme de generation du labirynthe : c'est une fonction récursive.
	 * 
	 * @param x1
	 *            coordonnee x de l'angle en haut a gauche de la salle
	 * @param y1
	 *            coordonnee y de l'angle en haut a gauche de la salle
	 * @param x2
	 *            coordonnee x de l'angle en bas a droite de la salle
	 * @param y2
	 *            coordonnee y de l'angle en bas a droite de la salle
	 * @param minSize
	 *            : taille minimum d'une salle (hauteur et largeur)
	 * @param direction
	 *            0: horizontal, 1: vertical
	 */
	private Salle divise(int x1, int y1, int x2, int y2, int minSize) {
		// affichageLevel();

		// Si on ne peut plus diviser la salle en deux sans obtenir deux
		// nouvelles salles d'une taille supérieure a minSize : on atteind le
		// dernier niveau de la récursivité et on génère une vraie salle dans
		// cette salle
		if ((x2 - x1 + 1 <= 2 * minSize) && (y2 - y1 + 1 <= 2 * minSize)) {
			// On genere des dimensions aleatoires pour la salle
			int largeur = aleatoire(minSize, x2 - x1 + 1);
			int hauteur = aleatoire(minSize, y2 - y1 + 1);

			// On genere une position aleatoire de notre vraie salle a
			// l'interieur de la salle
			int x = aleatoire(x1, x2 - largeur + 1);
			int y = aleatoire(y1, y2 - hauteur + 1);

			// On remplit la partie interieur de la salle sur le levelMap
			// (different des murs)
			remplissage(blocSol, x + 1, y + 1, largeur - 2, hauteur - 2);

			// On cree l'objet salle correspondant a cette salle, qu'on ajoute a
			// la liste de salle puis qu'on return
			Salle salleFinie = new Salle(x, y, x + largeur - 1, y + hauteur - 1);
			listeSalle.add(new Salle(x * 2, y * 2, (x + largeur - 1) * 2, (y
					+ hauteur - 1) * 2));
			return salleFinie;
		}

		int direction;

		// On regarde si une des coupes est impossible, dans ce cas on prend
		// l'autre, si les deux sont possibles on choisi au hasard
		if (x2 - x1 + 1 <= 2 * minSize) {
			direction = 1;
		} else if (y2 - y1 + 1 <= 2 * minSize) {
			direction = 0;
		} else {
			direction = aleatoire(0, 1);
		}

		Salle salle1, salle2;
		// En fonction de la direction on coupe deux nouvelles salles filles
		// dans la salle mère
		if (direction == 0) {
			int x = aleatoire(x1 + minSize, x2 - minSize + 1);
			salle1 = divise(x1, y1, x - 1, y2, minSize);
			salle2 = divise(x, y1, x2, y2, minSize);

			// On genere un couloir entre les deux salles filles
			couloirHorizontal(salle1, salle2);
		} else {
			int y = aleatoire(y1 + minSize, y2 - minSize + 1);
			salle1 = divise(x1, y1, x2, y - 1, minSize);
			salle2 = divise(x1, y, x2, y2, minSize);

			// On genere un couloir entre les deux salles filles
			couloirVertical(salle1, salle2);
		}

		// On retourne une salle en fonction des coordonnées des salles qui la
		// composent
		return new Salle(Math.min(salle1.x1, salle2.x1), Math.min(salle1.y1,
				salle2.y1), Math.max(salle1.x2, salle2.x2), Math.max(salle1.y2,
				salle2.y2));
	}

	/**
	 * @param salle1
	 * @param salle2
	 */
	public void couloirHorizontal(Salle salle1, Salle salle2) {
		if ((salle1.y2 - 1 < salle2.y1 + 1) || (salle2.y2 - 1 < salle1.y1 + 1)) {
			System.out
					.println("C'est ici en fait que ça marche pas Horizontal");
		} else {
			int minY = Math.max(salle1.y1 + 1, salle2.y1 + 1);
			int maxY = Math.min(salle1.y2 - 1, salle2.y2 - 1);
			int yCouloir = aleatoire(minY, maxY);
			int xDebut = 0, xFin = 0;
			for (int i = salle2.x1; i > salle1.x1; i--) {
				if (levelMap[yCouloir][i] == blocSol
						|| levelMap[yCouloir + 1][i] == blocSol
						|| levelMap[yCouloir - 1][i] == blocSol) {
					xDebut = i;
					break;
				}
			}
			for (int i = salle1.x2; i < salle2.x2; i++) {
				if (levelMap[yCouloir][i] == blocSol
						|| levelMap[yCouloir + 1][i] == blocSol
						|| levelMap[yCouloir - 1][i] == blocSol) {
					xFin = i;
					break;
				}
			}
			remplissage(blocSol, xDebut, yCouloir, xFin - xDebut + 1, 1);

			/*
			 * if(xFin < xDebut){ System.out.println("salle1 x1 = " + salle1.x1
			 * + "salle2 x1 = " + salle2.x1); System.out.println("salle1 x2 = "
			 * + salle1.x2 + "salle2 x2 = " + salle2.x2); }
			 * System.out.println("Horizontal" + xDebut + " " + xFin + " " +
			 * yCouloir);
			 */
		}
	}

	/**
	 * @param salle1
	 * @param salle2
	 */
	public void couloirVertical(Salle salle1, Salle salle2) {
		if ((salle1.x2 - 1 < salle2.x1 + 1) || (salle2.x2 - 1 < salle1.x1 + 1)) {
			System.out.println("C'est ici en fait que ça marche pas Vertical");

		} else {
			int minX = Math.max(salle1.x1 + 1, salle2.x1 + 1);
			int maxX = Math.min(salle1.x2 - 1, salle2.x2 - 1);
			int xCouloir = aleatoire(minX, maxX);
			int yDebut = 0, yFin = 0;

			for (int i = salle2.y1; i > salle1.y1; i--) {
				if (levelMap[i][xCouloir] == blocSol
						|| levelMap[i][xCouloir + 1] == blocSol
						|| levelMap[i][xCouloir - 1] == blocSol) {
					yDebut = i;
					break;
				}
			}
			for (int i = salle1.y2; i < salle2.y2; i++) {
				if (levelMap[i][xCouloir] == blocSol
						|| levelMap[i][xCouloir + 1] == blocSol
						|| levelMap[i][xCouloir - 1] == blocSol) {
					yFin = i;
					break;
				}
			}
			remplissage(blocSol, xCouloir, yDebut, 1, yFin - yDebut + 1);

			// System.out.println("Vertical" + yDebut + " " + yFin + " " +
			// xCouloir);
		}
	}

	/**
	 * Genere une case entree dans le labyrinthe
	 */
	public void generateEntree() {
		for (int y = 1; y < tailleHauteur - 2; y++) {
			for (int x = 1; x < tailleLargeur - 2; x++) {
				if (levelMap[y - 1][x - 1] == blocMur
						&& levelMap[y - 1][x] == blocMur
						&& levelMap[y - 1][x + 1] == blocMur
						&& levelMap[y][x - 1] == blocMur
						&& levelMap[y][x + 1] == blocMur
						&& levelMap[y + 1][x] == blocSol) {
					levelMap[y][x] = blocEscalierEntree;
					return;
				}
			}
		}
	}

	/*
	 * public void generateEntree() { // On recherche en partant d'en haut a
	 * gauche dans notre level une case // correspondant a nos criteres pour
	 * devenir une case entree for (int y = 2; y < tailleHauteur - 2; y++) { for
	 * (int x = 1; x < tailleLargeur - 2; x++) { if (levelMap[y + 1][x - 1] ==
	 * blocMur && levelMap[y + 1][x] == blocMur && levelMap[y + 1][x + 1] ==
	 * blocMur && levelMap[y][x - 1] == blocMur && levelMap[y][x + 1] == blocMur
	 * && levelMap[y - 1][x] == blocSol) { levelMap[y][x] = blocEscalierEntree;
	 * return; } } } }
	 */

	/**
	 * Genere une case sortie dans le labyrinthe
	 */
	public void generateSortie() {
		// On recherche en partant d'en bas a droite dans notre level une case
		// correspondant a nos criteres pour devenir une case sortie
		for (int y = tailleHauteur - 2; y > 0; y--) {
			for (int x = tailleLargeur - 2; x > 0; x--) {
				if (levelMap[y - 1][x - 1] == blocMur
						&& levelMap[y - 1][x] == blocMur
						&& levelMap[y - 1][x + 1] == blocMur
						&& levelMap[y][x - 1] == blocMur
						&& levelMap[y][x + 1] == blocMur
						&& levelMap[y + 1][x] == blocSol) {
					levelMap[y][x] = blocEscalierSortie;
					return;
				}
			}
		}
	}

	public char[][] getLevelMap() {
		return levelMap;
	}

	public void setLevelMap(char[][] levelMap) {
		this.levelMap = levelMap;
	}

	public int getTailleHauteur() {
		return tailleHauteur;
	}

	public void setTailleHauteur(int tailleHauteur) {
		this.tailleHauteur = tailleHauteur;
	}

	public int getTailleLargeur() {
		return tailleLargeur;
	}

	public void setTailleLargeur(int tailleLargeur) {
		this.tailleLargeur = tailleLargeur;
	}

	public void setCase(int colonne, int ligne, char character) {
		levelMap[ligne][colonne] = character;
	}

	public char getCase(int colonne, int ligne) {
		return levelMap[ligne][colonne];
	}

}
