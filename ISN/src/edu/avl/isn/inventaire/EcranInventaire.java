package edu.avl.isn.inventaire;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import edu.avl.isn.EcranJeu;

/**
 * Gere l'ecran qui affiche l'inventaire
 * 
 * @author vgalves
 * 
 */
public class EcranInventaire extends BasicGameState {

	public static final int ID = 4;

	private StateBasedGame game;

	/**
	 * Initialise l'ecran
	 */
	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {
		this.game = game;
	}

	/**
	 * Rend l'ecran
	 */
	@Override
	public void render(GameContainer arg0, StateBasedGame arg1, Graphics g)
			throws SlickException {
		g.drawString("Ecran Inventaire", 200, 200);

	}

	/**
	 * Met a jour l'ecran
	 */
	@Override
	public void update(GameContainer arg0, StateBasedGame arg1, int arg2)
			throws SlickException {
		// TODO Auto-generated method stub

	}

	/**
	 * Si une touche est appuyee
	 */
	@Override
	public void keyPressed(int key, char c) {
		switch (key) {
		case Input.KEY_I:
			game.enterState(EcranJeu.ID);
			break;

		default:
			break;
		}
	}

	/**
	 * renvoi l'ID
	 */
	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return ID;
	}

}
