package edu.avl.isn.inventaire;

import java.awt.Font;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import edu.avl.isn.EcranJeu;

/**
 * Gere l'ecran de la boutique<p>
 * Le fond a ete fait par alex
 * 
 * @author vgalves
 *
 */

public class EcranBoutique extends BasicGameState {
	public static final int ID = 2;

	private StateBasedGame game;
	
	
	//L'argent que l'on possede
	private int money;
	
	//La police
	private Font font;
	private TrueTypeFont ttf;
	
	//L'image de la piece, de fond et de boutique
	private Image goldImage;
	private Image fond;
	private Image fondBois;
	
	//La liste de nos Icon qu'on peu acheter
	private ArrayList<Icon> tabObj = new ArrayList<Icon>();
	
	//Le debut des Icon x, y et l'espacement entre les Icon
	private int startX = 200,startY = 100,espacement = 100;
	
	//L'image pour quitter
	private Image imExit;
	

	/**
	 * Initialise l'ecran de la boutique
	 */
	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {
		
		//Chargement des images de fond
		this.fond = new Image("sprite/imBoutique/image fond.jpg");
		this.fondBois = new Image("sprite/imBoutique/fondBoutique.png");
		this.goldImage = new Image("sprite/imBoutique/coin.png");
		
		//On charge les donnee aisni que les image da chaque Icon
		chargerIcon("sprite/imBoutique/imObj/tabIcon.txt");
		//On charge leur position
		chargerPos();
		
		this.game = game;
		
		//On charge l'argent qu'on a
		this.money = EcranJeu.getPersoMoney();
		
		//On charge l'image de sortie
		this.imExit = new Image("sprite/imBoutique/x.png");
		
		//On initialise la police pour ecrire l'argent qu'on a
		font = new Font("Verdana", Font.BOLD, 30);
	    ttf = new TrueTypeFont(font, true);
	}

	/**
	 * Rend l'ecran boutique
	 */
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {
		//On affiche les image de fond, de sortie et d'argent
		g.drawImage(fond, 0, 0);
		g.drawImage(fondBois, 0, 0);
		g.drawImage(imExit, 700, 20);
		g.drawImage(goldImage, 140, 10);
		
		//On indique qu'on est dans la boutique
		ttf.drawString(200, 20, String.valueOf(money), Color.yellow);
		
		//On indique qu'on est dans la boutique
		g.drawString("BOUTIQUE", 50, 50);
		
		//On affiche les Icon
		renderTabIco(g);
		
		
	}
	
	/**
	 * On affiche chaque Icon un par un
	 * @param g Graphics
	 */
	public void renderTabIco(Graphics g){
		for (int i = 0; i < tabObj.size(); i++) {
			tabObj.get(i).renderIcon(g);
		}
	}

	/**
	 * mise a jour de l'ecran
	 */
	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * renvoi l'id
	 */
	@Override
	public int getID() {
		return ID;
	}

	/**
	 * Quand on bouge la souris
	 * <p>
	 * TODO Quand on passe la souris sur la croix
	 */
	@Override
	public void mouseMoved(int oldx, int oldy, int newx, int newy) {
		mouseOn(newx, newy);
	}
	
	/**
	 * On On rafraichit l'Icon sur laquelle la souris est
	 * @param posx de la souris
	 * @param posy de la souris
	 */
	public void mouseOn (int posx, int posy){
		//On parcour la liste des Icon
		for (int i = 0; i < tabObj.size(); i++) {
			//On verifie si la souris est dessus
			if (posx >= tabObj.get(i).posx && posx <= tabObj.get(i).posx + espacement
					&& posy >= tabObj.get(i).posy && posy <= tabObj.get(i).posy + espacement){
				tabObj.get(i).setMouseOn(true);
			}
			else {
				tabObj.get(i).setMouseOn(false);
			}
		}
	}
	
	/**
	 * Quand on clique sur la croix, on 
	 * retourne au menu principal
	 * <p>TODO Quand on clique sur un Icon, faire en sorte qu'on l'achete
	 */
	@Override
	public void mouseClicked(int button, int x, int y, int clickCount) {
		if (x <= 800 && x>=728 && y>=48 && y<=120){
			game.enterState(EcranJeu.ID);
		}
	}
	
	/**
	 * Permet d'initialiser le tabIcon de la boutique
	 * <p>Les images sont dans le dossier Sprite/imBoutique/imObj
	 * <p>Le fichier contenat les infos sur les objets sont dans
	 * @param nomFichier
	 * @throws SlickException
	 */
	public void chargerIcon(String nomFichier) throws SlickException {
		
		
		try {
			//Lecture du fichier
			InputStream ips=new FileInputStream(nomFichier); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String ligne;
			int noLigne = 0;
			//A chaque ligne, un icon
			while ((ligne=br.readLine())!=null){
				Icon tpIcon = new Icon(ligne, noLigne);
				tabObj.add(tpIcon);
				noLigne++;
			}	
			br.close(); 
			

			//Les erreur que l'on peut rencontrer dans cette maneuvre
		} catch (FileNotFoundException e) {	
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Permet d'associer a chaque Icon une position dans la fenetre
	 */
	public void chargerPos(){
		
		int no = 0;
		int ligne = 1;
		do {
			for (int i = 0; i < 5; i++) {
				if (no < tabObj.size()) {
					tabObj.get(no).setPosx(i * espacement + startX);
					tabObj.get(no).setPosy(ligne * espacement + startY);
					no++;
				}
			}
			ligne++;
		} while (no < tabObj.size());
		
	}
	
	/**
	 * Gere un Icon
	 * @author vgalves
	 *
	 */
	class Icon{
		
		private Image im;
		private String name;
		private int prix;
		/**
		 * Le type d'objet, si c'est une epe, une lance ou un arc
		 */
		private int prop;
		private String type;
		private boolean isMouseOn;
		private int posx, posy;
		
		/**
		 * Construit un nouvel Icon
		 * @param ligne
		 * @param t le numero de l'image a utiliser
		 * @throws SlickException
		 */
		public Icon (String ligne, int t) throws SlickException{
			String[] arrayLigne;
			init() ;
			this.im = new Image("sprite/imBoutique/imObj/"+String.valueOf(t)+".png");
			arrayLigne = ligne.split(";");
			for (int i = 0; i < arrayLigne.length; i++) {
				String tpString = arrayLigne[i];
				switch (i) {
				
				case 0:
					this.name = tpString;
					break;
				case 1:
					try {
						this.prix = Integer.parseInt(tpString);
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					break;
				case 2:
					try {
						this.prop = Integer.parseInt(tpString);
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				case 3:
					this.type = tpString;
					break;

				default:
					break;
				}
				
			}
			
		}
		
		/**
		 * Intitialise l'icon
		 */
		public void init(){
			this.im = null;
			this.name = "";
			this.prix = 0;
			this.type = "";
			this.posx = 0;
			this.posy = 0;
		}
		
		/**
		 * Rend un Icon
		 * @param g
		 */
		public void renderIcon(Graphics g){
			g.drawImage(im, posx, posy);
			if (isMouseOn) {
				g.setColor(new Color(170, 70, 20, 255));
				g.fillRect(posx-20, posy-20, 150, 50);
				g.setColor(Color.black);
				g.drawString(name, posx - 10, posy -18);
				g.drawString("Cout : " + String.valueOf(prix), posx - 10, posy - 5);
				if (type.equals("Lance") || type.equals("Epee")||type.equals("Arc")) {
					g.drawString("+"+ String.valueOf(prop)+" en attaque", posx - 10, posy + 8);
				}
				if (type.equals("Armure") ) {
					g.drawString("+"+ String.valueOf(prop)+" en defense", posx - 10, posy + 8);
				}
				if (type.equals("Xp") || type.equals("Vie")||type.equals("Mana")) {
					g.drawString("+"+ String.valueOf(prop)+" en " + type, posx - 10, posy + 8);
				}
			}
			
		}


		/**
		 * @param isMouseOn the isMouseOn to set
		 */
		public void setMouseOn(boolean isMouseOn) {
			this.isMouseOn = isMouseOn;
		}


		/**
		 * @param posx the posx to set
		 */
		public void setPosx(int posx) {
			this.posx = posx;
		}


		/**
		 * @param posy the posy to set
		 */
		public void setPosy(int posy) {
			this.posy = posy;
		}
	}
}
