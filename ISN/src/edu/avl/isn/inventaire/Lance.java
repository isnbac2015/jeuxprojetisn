package edu.avl.isn.inventaire;

import org.newdawn.slick.Graphics;

import edu.avl.isn.creature.Monstre;

public class Lance extends Arme {

	

	public Lance(String nomObj, String ref, int spx, int spy, int degats) {
		super(nomObj, ref, "lance", spx, spy, degats);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void infligerDegats(Monstre mo) {
		mo.perdreVie(degats);
		
	}

	@Override
	void render(Graphics g) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected String getType() {
		return typeObj;
	}
	
	

}
