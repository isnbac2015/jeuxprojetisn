/**
 * 
 */
package edu.avl.isn.inventaire;

import edu.avl.isn.creature.Monstre;

/**
 * Detemine le type d'objet qui inflige des degats
 * @author vicga_000
 *
 */
public abstract class Arme extends Obj {
	



	protected int degats;
	
	/**
	 * Permet de construire un objet arme
	 * @param nomObj
	 * @param ref
	 * @param typeObjet
	 * @param spx
	 * @param spy
	 * @param degats
	 */
	public Arme(String nomObj, String ref, String typeObjet, int spx, int spy, int degats) {
		super(nomObj,typeObjet, ref,  spx, spy);
		this.degats = degats;
		// TODO Auto-generated constructor stub
	}
	
	public abstract void infligerDegats (Monstre mo);

	
	
	

}
