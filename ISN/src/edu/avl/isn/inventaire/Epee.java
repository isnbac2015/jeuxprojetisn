package edu.avl.isn.inventaire;

import org.newdawn.slick.Graphics;

import edu.avl.isn.creature.Monstre;

public class Epee extends Arme {

	

	public Epee(String nomObj, String ref , int spx, int spy, int degats) {
		super(nomObj, ref, "epee", spx, spy, degats);
		// TODO Auto-generated constructor stub
	}

	@Override
	void render(Graphics g) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void infligerDegats(Monstre mo) {
		mo.perdreVie(degats);
		
	}
@Override
	protected String getType() {
		return typeObj;
	}

	

}
