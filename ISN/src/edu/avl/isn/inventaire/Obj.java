/**
 * 
 */
package edu.avl.isn.inventaire;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

/**
 * Classe abstaite qui defini un objet
 * @author vicga_000
 *
 */
public abstract class Obj {
	
	protected SpriteSheet sp;
	/**
	 * L'image qui sera afficher dans l'inventaire
	 */
	protected Image imInventaire;
	protected String nomObj;
	protected String typeObj;
	
	/**
	 * Construit un objet
	 * @param nomObj
	 * @param typeObjet
	 * @param ref la reference du sp
	 * @param spx la coordonnee x de l'image dans le sp
	 * @param spy la coordonnee y de l'image dans le sp
	 */
	public Obj(String nomObj,String typeObjet, String ref, int spx, int spy) {
		super();
		this.nomObj = nomObj;
		this.typeObj = typeObjet;
		
		try {
			this.sp = new SpriteSheet(ref, 16, 16);
			this.imInventaire = sp.getSprite(spx, spy);
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * La methode pour rendre l'objet
	 * @param g
	 */
	abstract void render(Graphics g);



	/**
	 * @return the imInventaire
	 */
	public Image getImInventaire() {
		return imInventaire;
	}



	/**
	 * @param imInventaire the imInventaire to set
	 */
	public void setImInventaire(Image imInventaire) {
		this.imInventaire = imInventaire;
	}



	/**
	 * @return the nomObj
	 */
	public String getNomObj() {
		return nomObj;
	}
	/**
	 * 
	 * @return the obj type
	 */
	protected abstract String getType();





/**
 * 
 * @return
 */

	public String getTypeObj() {
		return typeObj;
	}






/**
 * 
 * @param typeObj
 */
	public void setTypeObj(String typeObj) {
		this.typeObj = typeObj;
	}

}
