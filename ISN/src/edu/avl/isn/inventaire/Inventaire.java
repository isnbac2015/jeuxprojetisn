package edu.avl.isn.inventaire;

import java.util.ArrayList;

import org.newdawn.slick.Graphics;

/**
 * Gere l'inventaire
 * <p>pas encore utilisee
 * @author vgalves
 *
 */
public class Inventaire {
	//Liste d'objet selon leurs type
	private ArrayList<Epee> listeEpee;
	private ArrayList<Arc> listeArc;
	private ArrayList<Lance> listeLance;
	
	/**
	 * Cree l'inventaire
	 */
	public Inventaire(){
		this.listeEpee = new ArrayList<Epee>();
		this.listeArc = new ArrayList<Arc>();
		this.listeLance = new ArrayList<Lance>();
		
	}
	/**
	 * Affiche l'inventaire
	 * @param g
	 */
	public void afficherInventaire (Graphics g){
		
	}
	
	/**
	 * Ajoute dans l'inventaire
	 * 
	 * @WARNING pas sure que ca marche!!!
	 * @param obj
	 */
	public void ajouterDansInventaire(Obj obj){
		//Pas sur que ca marche
		if(obj.getType().equals("epee"))
			listeEpee.add((Epee) obj);
		if(obj.getType().equals("arc"))
			listeArc.add((Arc) obj);
		if(obj.getType().equals("lance"))
			listeLance.add((Lance) obj);
	}

}
