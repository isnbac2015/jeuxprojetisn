/**
 * 
 */
package edu.avl.isn.creature;

import org.newdawn.slick.Animation;
import org.newdawn.slick.SpriteSheet;

/**
 * @author vicga_000
 *
 */
public abstract class Creature {
	protected Animation[] animations;
	
	//Entre 0 et 100
	protected float vie = 100f;
	protected float attack;
	
	protected boolean isAlive;
	
	// 0< defense <=1 sachant que un ne protege pas et 0 est un protection complete, mais ne peut etre atteint.
	protected float defense;
	
	protected Animation loadAnimation(SpriteSheet spriteSheet, int startX,
			int endX, int y, int duration) {
		Animation animation = new Animation();
		for (int x = startX; x <= endX; x++) {
			animation.addFrame(spriteSheet.getSprite(x, y), duration);
		}
		return animation;
	}
	
	public void perdreVie (float degats){
		this.vie = vie - (defense*degats);
	}

	/**
	 * @return the vie
	 */
	public float getVie() {
		return vie;
	}

	/**
	 * @param vie the vie to set
	 */
	public void setVie(float vie) {
		this.vie = vie;
	}

	/**
	 * @return the attack
	 */
	public float getAttack() {
		return attack;
	}

	/**
	 * @param attack the attack to set
	 */
	public void setAttack(float attack) {
		this.attack = attack;
	}

	/**
	 * @return the defense
	 */
	public float getDefense() {
		return defense;
	}

	/**
	 * @param defense the defense to set
	 */
	public void setDefense(float defense) {
		this.defense = defense;
	}

}
