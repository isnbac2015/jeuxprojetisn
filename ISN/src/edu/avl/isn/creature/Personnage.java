package edu.avl.isn.creature;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

public class Personnage extends Creature {

	
	
	private float xp, mana ;
	private int money;
	private int niveau = 1;

	// creation d'un tableau contenant les differentes animations de notre
	// personnages

	public Personnage() throws SlickException {
		this.animations = new Animation[20];
		SpriteSheet spriteSheet = new SpriteSheet("sprite/character.png", 64, 64);
		this.animations[0] = loadAnimation(spriteSheet, 0, 0, 0, 50);
		this.animations[1] = loadAnimation(spriteSheet, 0, 0, 1, 50);
		this.animations[2] = loadAnimation(spriteSheet, 0, 0, 2, 50);
		this.animations[3] = loadAnimation(spriteSheet, 0, 0, 3, 50);
		this.animations[4] = loadAnimation(spriteSheet, 1, 8, 0, 50);
		this.animations[5] = loadAnimation(spriteSheet, 1, 8, 1, 50);
		this.animations[6] = loadAnimation(spriteSheet, 1, 8, 2, 50);
		this.animations[7] = loadAnimation(spriteSheet, 1, 8, 3, 50);
		
		spriteSheet = new SpriteSheet("sprite/perso-attack-bow.png", 64, 64);
		this.animations[8] = loadAnimation(spriteSheet, 0, 12, 16, 80);
		this.animations[9] = loadAnimation(spriteSheet, 0, 12, 17, 80);
		this.animations[10] = loadAnimation(spriteSheet, 0, 12, 18, 80);
		this.animations[11] = loadAnimation(spriteSheet, 0, 12, 19, 80);
		
		spriteSheet = new SpriteSheet("sprite/perso-attack-sword.png", 192, 192);
		this.animations[12] = loadAnimation(spriteSheet, 0, 5, 7, 80);
		this.animations[13] = loadAnimation(spriteSheet, 0, 5, 8, 80);
		this.animations[14] = loadAnimation(spriteSheet, 0, 5, 9, 80);
		this.animations[15] = loadAnimation(spriteSheet, 0, 5, 10, 80);
		
		spriteSheet = new SpriteSheet("sprite/perso-attack-lance.png", 64, 64);
		this.animations[16] = loadAnimation(spriteSheet, 0, 7, 4, 80);
		this.animations[17] = loadAnimation(spriteSheet, 0, 7, 5, 80);
		this.animations[18] = loadAnimation(spriteSheet, 0, 7, 6, 80);
		this.animations[19] = loadAnimation(spriteSheet, 0, 7, 7, 80);
		
		
		this.vie = 50f;
		this.mana = 38f;
		this.xp = 100f;
		
		@SuppressWarnings("unused")
		int money = 0;
	}

	public void render(GameContainer container, Graphics g, float x, float y, int direction, boolean moving, boolean useSword, boolean useBow,boolean useLance)
			throws SlickException {
		g.drawAnimation(animations[direction + (moving ? 4 : (useBow ? 8 : (useSword ? 12 :(useLance ?16 : 0)))) ],
				x - (useSword ? 96 - 16 : 16),	y - (useSword ? (96 + 7) : 38));
	}

	

	

	/**
	 * @return the xp
	 */
	public float getXp() {
		return xp;
	}

	/**
	 * @param xp the xp to set
	 */
	public void setXp(float xp) {
		this.xp = xp;
	}

	/**
	 * @return the mana
	 */
	public float getMana() {
		return mana;
	}

	/**
	 * @param mana the mana to set
	 */
	public void setMana(float mana) {
		this.mana = mana;
	}

	/**
	 * @return the money
	 */
	public int getMoney() {
		return money;
	}

	/**
	 * @param money the money to set
	 */
	public void setMoney(int money) {
		this.money = money;
	}

	public int getNiveau() {
		return niveau;
	}

	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}
	
	

}
