package edu.avl.isn;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

/**
 * @author vgalves
 *
 */
public class Main {
	
	public static int tailleFenetreX = 1024; //largeur de la fenetre
	public static int tailleFenetreY = 768; //hauteur de la fenetre
	
	/**
	 * LA merthode principale
	 * @param args
	 * @throws SlickException
	 */
	public static void main(String[] args) throws SlickException {
		//on cree une fenetetre appelee Jeu
		AppGameContainer app = new AppGameContainer(new StateGame("JEU"));
		
		//Nombre d'image par seconde
		app.setTargetFrameRate(60);
		
	
		
		app.setMusicOn(true);

		//On autorise l'affichage de la fenetre a l'ecran
		app.setDisplayMode(tailleFenetreX, tailleFenetreY, false); 

		//on lance notre fenetre
		app.start(); 
	}
}


