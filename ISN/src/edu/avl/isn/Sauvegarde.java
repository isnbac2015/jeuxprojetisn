package edu.avl.isn;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import org.newdawn.slick.SlickException;

/**
 * Suvegarde et restaure  le niveau
 * @author louis
 *
 */
public class Sauvegarde {
	/**
	 * Sauvegarde le niveau dans "test.txt"
	 * @param niveau
	 */
	public static void saveNiveau(ArrayList<Niveau> niveaux) {
		{
			try {
				FileOutputStream fos = new FileOutputStream("test.txt");
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				oos.writeObject(niveaux);
				oos.close();
				fos.close();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}

	/**
	 * Restaure le niveau depuis "test.txt"
	 * @return le niveau charger
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SlickException
	 * @throws FileNotFoundException
	 */
	public static ArrayList<Niveau> restaureNiveau() throws IOException, ClassNotFoundException, SlickException,FileNotFoundException {
			FileInputStream fis = new FileInputStream("test.txt");
			ObjectInputStream ois = new ObjectInputStream(fis);
			ArrayList<Niveau> niveaux = (ArrayList<Niveau>) ois.readObject();
			ois.close();
			fis.close();
			for(int i = 0; i < niveaux.size(); i++){
				niveaux.get(i).setObjetSprite();
				niveaux.get(i).setBlocSprite();
			}
		return niveaux;
	}
}
