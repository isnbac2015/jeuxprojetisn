package edu.avl.isn;

import java.io.Serializable;
import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

import edu.avl.isn.creature.Personnage;

/**
 * Gere la map
 * 
 * @author vgalves
 * 
 */
@SuppressWarnings("serial")
public class Map implements Serializable {

	// protected SpriteSheet blocSprite;

	Random rand = new Random();

	// creation d'un tableau contenant des cases
	private Case[][] map;

	// taille de chaque etage
	private int mapSizeX, mapSizeY;

	private int etage;

	// position de l'entree et de la sortie
	public int entreeY, entreeX, sortieX, sortieY;

	/**
	 * Constructeur de l'objet map : tableau a deux entrees contenant des cases
	 * 
	 * @param level
	 *            Le level a partir du quel on genere la map
	 * @param sizeX
	 *            La longueur de la map
	 * @param sizeY
	 *            La hauteur de la map
	 */
	public Map(Level level, int sizeX, int sizeY, int etage)
			throws SlickException {
		this.etage = etage;
		mapSizeX = sizeX;
		mapSizeY = sizeY;
		level.affichageLevel();

		// On genere une map a partir de level
		generateMap(level);

		// obsolete : associerObjet();

		// On associe a chaque case les coordonnees de l'image correspondante
		associerImage();

		// On prepare les images des cases
		// blocSprite = new SpriteSheet("sprite/fond.png", 32, 32);
	}

	/**
	 * Fonction associant une image a chaque case : elle parcours ligne par
	 * ligne la map et si elle trouve une case accessible alors elle associe des
	 * images aux cases autours de celle-ci
	 */
	public void associerImage() {
		for (int y = 2; y <= mapSizeY - 2; y++) {
			for (int x = 2; x <= mapSizeX - 2; x++) {
				if (map[y][x].accessible) {
					if (!map[y][x - 1].accessible) {
						if (!map[y - 1][x].accessible) {
							map[y - 1][x - 1] = new Case(3, 1, false);
							map[y - 2][x - 1] = new Case(4, 0, false);
							if (y - 3 >= 0) {
								if (map[y - 3][x - 1].accessible) {
									map[y - 2][x - 1] = new Case(6, 0, false);
								}
							}
						}
						if (map[y - 1][x - 1].accessible) {
							map[y][x - 1] = new Case(3, 0, false);
						} else {
							map[y][x - 1] = new Case(3, 1, false);
						}
						if (!map[y + 1][x - 1].accessible) {
							map[y + 1][x - 1] = new Case(4, 2, false);
						}
					}
					if (!map[y][x + 1].accessible) {
						if (!map[y - 1][x].accessible) {
							map[y - 1][x + 1] = new Case(1, 1, false);
							map[y - 2][x + 1] = new Case(5, 0, false);
							if (y - 3 >= 0) {
								if (map[y - 3][x + 1].accessible) {
									map[y - 2][x + 1] = new Case(6, 1, false);
								}
							}
						}
						if (map[y - 1][x + 1].accessible) {
							map[y][x + 1] = new Case(1, 0, false);
						} else {
							map[y][x + 1] = new Case(1, 1, false);
						}
						if (!map[y + 1][x + 1].accessible) {
							map[y + 1][x + 1] = new Case(4, 1, false);
						}
					}

					if (!map[y - 1][x].accessible) {

						if (map[y - 1][x - 1].accessible) {
							map[y - 1][x] = new Case(1, 4, false);
							map[y - 2][x] = new Case(1, 3, false);

						} else if (!map[y][x - 1].accessible) {
							map[y - 1][x] = new Case(1, 4, false);
							map[y - 2][x] = new Case(4, 3, false);

						} else {
							if (map[y - 1][x + 1].accessible) {
								map[y - 1][x] = new Case(3, 4, false);
								map[y - 2][x] = new Case(3, 3, false);
							} else if (!map[y][x + 1].accessible) {
								map[y - 1][x] = new Case(3, 4, false);
								map[y - 2][x] = new Case(5, 3, false);
							} else {
								int aleatoire = aleatoire(1, 20);
								if (aleatoire == 1) {
									map[y - 1][x] = new Case(2, 5, false);
									map[y - 2][x] = new Case(2, 3, false);

								} else if (2 <= aleatoire && aleatoire <= 4) {
									map[y - 1][x] = new Case(3, 6, false);
									map[y - 2][x] = new Case(3, 5, false);

								} else {
									map[y - 1][x] = new Case(2, 4, false);
									map[y - 2][x] = new Case(2, 3, false);
								}

							}
						}
					}

					if (!map[y + 1][x].accessible) {
						map[y + 1][x] = new Case(2, 0, false);
					}
				}
			}
		}
		imageSortie();
		imageEntree();
	}

	/**
	 * Permet de generer une map a partir d'un objet level
	 * 
	 * @param level
	 *            Le level a partir du quel on genere la map
	 */
	public void generateMap(Level level) {
		map = new Case[mapSizeY][mapSizeX];

		for (int x = 0; x < mapSizeX; x++) {
			for (int y = 0; y < mapSizeY; y++) {
				map[y][x] = associerCaseImage(level.getCase(x / 2, y / 2));

				if (level.getCase(x / 2, y / 2) == Level.blocEscalierEntree) {
					entreeY = y;
					entreeX = x;
				}
				if (level.getCase(x / 2, y / 2) == Level.blocEscalierSortie) {
					sortieY = y;
					sortieX = x;
				}
			}
		}
	}

	public void imageSortie() {
		int x = sortieX;
		int y = sortieY;
		map[y][x] = new Case(4, 4, true);
		map[y][x - 1] = new Case(4, 4, true);
		map[y - 1][x] = new Case(4, 4, true);
		map[y - 1][x - 1] = new Case(4, 4, true);
		map[y - 2][x] = new Case(5, 6, false);
		map[y - 2][x - 1] = new Case(4, 6, false);
		map[y - 3][x] = new Case(5, 5, false);
		map[y - 3][x - 1] = new Case(4, 5, false);
	}

	public void imageEntree() {
		int x = entreeX;
		int y = entreeY;

		if (etage == 0) {
			map[y][x] = new Case(2, 10, true);
			map[y][x - 1] = new Case(1, 10, true);
			map[y - 1][x] = new Case(2, 9, true);
			map[y - 1][x - 1] = new Case(1, 9, true);

			map[y - 2][x - 1] = new Case(1, 4, false);
			map[y - 2][x] = new Case(3, 4, false);
			map[y - 3][x - 1] = new Case(4, 3, false);
			map[y - 3][x] = new Case(5, 3, false);

			map[y - 2][x - 2] = new Case(3, 1, false);
			map[y - 2][x + 1] = new Case(1, 1, false);

			if (!map[y + 1][x - 2].accessible) {
				map[y][x - 2] = new Case(3, 1, false);
				map[y - 1][x - 2] = new Case(3, 1, false);
				map[y - 3][x - 2] = new Case(4, 0, false);
			} else {
				map[y][x - 2] = new Case(3, 4, false);
				map[y - 1][x - 2] = new Case(3, 3, false);
				map[y - 3][x - 2] = new Case(4, 0, false);
			}

			if (!map[y + 1][x + 1].accessible) {
				map[y][x + 1] = new Case(1, 1, false);
				map[y - 1][x + 1] = new Case(1, 1, false);
				map[y - 3][x + 1] = new Case(5, 0, false);
			} else {
				map[y][x + 1] = new Case(1, 4, false);
				map[y - 1][x + 1] = new Case(1, 3, false);
				map[y - 3][x + 1] = new Case(5, 0, false);
			}

		} else {
			map[y][x] = new Case(5, 10, true);
			map[y][x - 1] = new Case(4, 10, true);
			map[y - 1][x] = new Case(5, 9, false);
			map[y - 1][x - 1] = new Case(4, 9, false);
			
			if (!map[y + 1][x - 2].accessible) {
				map[y][x - 2] = new Case(3, 1, false);
				map[y - 1][x - 2] = new Case(4, 0, false);
			} else {
				map[y][x - 2] = new Case(3, 10, false);
				map[y - 1][x - 2] = new Case(3, 9, false);
			}

			if (!map[y + 1][x + 1].accessible) {
				map[y][x + 1] = new Case(1, 1, false);
				map[y - 1][x + 1] = new Case(5, 0, false);
			} else {
				map[y][x + 1] = new Case(6, 10, false);
				map[y - 1][x + 1] = new Case(6, 9, false);
			}
		}
	}

	/**
	 * associe a chaque case une image
	 * 
	 * @param c
	 * @return
	 */
	public Case associerCaseImage(char c) {
		/*
		 * if (c == Level.blocMur) { return new Case(2, 2, false); }
		 */
		if (c == Level.blocSol) {
			return new Case(1, 5, true);
		} else {
			return new Case(2, 2, false);
		}
		/*
		 * if (c == Level.blocEscalierEntree) { return new Case(1, 5, true); }
		 * if (c == Level.blocEscalierSortie) { return new Case(1, 5, true); }
		 * return new Case(2, 2, false);
		 */
	}

	/**
	 * rend la map case par case
	 * 
	 * @param container
	 *            le gameContainer
	 * @param g
	 *            l'objet graphic
	 * @param x
	 * @param y
	 * @param blocSprite
	 * @throws SlickException
	 */
	public void render(GameContainer container, Graphics g, float x, float y,
			SpriteSheet blocSprite) throws SlickException {

		int translateX = container.getWidth() / 2 - (int) x;
		int translateY = container.getHeight() / 2 - (int) y;

		translateY = Math.min(translateY, 0);
		translateX = Math.min(translateX, 0);
		translateX = Math.max(translateX, container.getWidth() - mapSizeX * 32);
		translateY = Math
				.max(translateY, container.getHeight() - mapSizeY * 32);

		g.translate(translateX, translateY);

		blocSprite.startUse();

		for (int i = 0; i < mapSizeX; i++) {
			for (int j = 0; j < mapSizeY; j++) {

				blocSprite.renderInUse(i * 32, j * 32, map[j][i].spriteX,
						map[j][i].spriteY);

			}
		}
		blocSprite.endUse();
	}

	/**
	 * Gere une case da la map
	 * 
	 * @author alex
	 * 
	 */
	class Case implements Serializable {

		int spriteY;
		int spriteX;
		boolean accessible;

		/**
		 * Cree une nouvelle case
		 * 
		 * @param spriteX
		 * @param spriteY
		 * @param accessible
		 */
		public Case(int spriteX, int spriteY, boolean accessible) {
			this.spriteX = spriteX;
			this.spriteY = spriteY;
			this.accessible = accessible;

		}

		/**
		 * Cree une nouvelle case
		 */
		public Case() {
			this.spriteX = 1;
			this.spriteY = 5;
			this.accessible = true;
		}

	}

	/**
	 * determine si la case de coordonnee x,y dans le tableau map[x][y] est
	 * accessible
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean estAccessible(int x, int y) {
		return map[y][x].accessible;
	}

	/**
	 * Retourne un nombre aleatoire entre min et max
	 * 
	 * @param min
	 * @param max
	 * @return
	 */
	public int aleatoire(int min, int max) {
		if (max - min + 1 <= 0) {
			System.out.println("erreur: min=" + min + " max=" + max);
		}
		return rand.nextInt(max - min + 1) + min;
	}
}
